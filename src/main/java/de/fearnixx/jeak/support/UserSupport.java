package de.fearnixx.jeak.support;

import de.fearnixx.jeak.IBot;
import de.fearnixx.jeak.event.IQueryEvent;
import de.fearnixx.jeak.event.bot.IBotStateEvent;
import de.fearnixx.jeak.reflect.*;
import de.fearnixx.jeak.service.locale.ILocalizationUnit;
import de.fearnixx.jeak.teamspeak.cache.IDataCache;
import de.fearnixx.jeak.teamspeak.data.IChannel;
import de.fearnixx.jeak.teamspeak.data.IClient;
import de.fearnixx.jeak.util.Configurable;
import de.fearnixx.jeak.voice.connection.IVoiceConnection;
import de.fearnixx.jeak.voice.connection.IVoiceConnectionService;
import de.fearnixx.jeak.voice.sound.AudioType;
import de.fearnixx.jeak.voice.sound.IAudioPlayer;
import de.mlessmann.confort.api.IConfig;
import de.mlessmann.confort.api.IConfigNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Stream;

/**
 * Created by MarkL4YG on 13-Dec-17
 * <p>
 * A support plugin
 */
@JeakBotPlugin(id = "usersupport", builtAgainst = "1.2.0")
public class UserSupport extends Configurable {

    private static final Logger logger = LoggerFactory.getLogger(UserSupport.class);
    private static final String CONF_STAFF_GROUPS_NODE = "staff_groups";
    private static final String CONF_STAFF_GROUPS_POKE_NODE = "staff_groups_poke";
    private static final String CONF_CHANNELS_NODE = "channels";
    private static final String CONF_HOTLINE = "hotline";
    private static final String CP_URI_PREFIX = "classpath:";

    private static final Random random = new Random();
    public static final String CONF_MUSIC = "music";

    @Inject
    private IBot bot;

    @Inject
    private IDataCache dataCache;

    @Inject
    @Config
    private IConfig myConfigRef;

    @Inject
    @LocaleUnit(value = "usersupport", defaultResource = "lang/usersupport.json")
    private ILocalizationUnit localizationUnit;

    @Inject
    private IVoiceConnectionService voiceService;
    private final List<IVoiceConnection> voiceConnections = new LinkedList<>();

    public UserSupport() {
        super(UserSupport.class);
    }

    @Override
    protected IConfig getConfigRef() {
        return myConfigRef;
    }

    @Listener
    public void onInitialize(IBotStateEvent.IInitializeEvent event) {
        if (!loadConfig()) {
            event.cancel();
        }
    }

    @Listener
    public void onConnected(IBotStateEvent.IConnectStateEvent.IPostConnect event) {
        getConfig().getNode(CONF_CHANNELS_NODE)
                .optMap()
                .ifPresent(map -> {
                    map.forEach((channelIdStr, node) -> {
                        if (!node.getNode(CONF_HOTLINE).isVirtual()) {
                            enableHotlineMusic(channelIdStr, node.getNode(CONF_HOTLINE));
                        }
                    });
                });
    }

    private void enableHotlineMusic(String channelIdStr, IConfigNode hotlineNode) {

        String path = hotlineNode.getNode(CONF_MUSIC).optString().orElse(null);
        List<String> paths = hotlineNode.getNode(CONF_MUSIC).optValueList(String.class);

        final List<String> musicSources = new ArrayList<>();
        if (path != null) {
            musicSources.add(path);
        } else if (!paths.isEmpty()) {
            musicSources.addAll(paths);
        } else {
            logger.warn("Hotline for channel \"{}\" not enabled: No music configured!", channelIdStr);
            return;
        }

        final Supplier<InputStream> randomStrm = () -> {
            String selectedPath = musicSources.get(random.nextInt(musicSources.size()));
            return generateInputStreamFromFile(selectedPath);
        };

        final String voiceIdentifier = "usersupport:" + channelIdStr;
        String nickname = hotlineNode.getNode("nickname").optString(voiceIdentifier);
        Double targetVolume = hotlineNode.getNode("volume").optDouble(.01);

        voiceService.requestVoiceConnection(voiceIdentifier, c -> {
            voiceConnections.add(c);
            c.setShouldForwardTextMessages(true);
            c.connect(() -> {
                c.setClientNickname(nickname);
                c.sendToChannel(Integer.parseInt(channelIdStr));
                IAudioPlayer player = c.registerAudioPlayer(AudioType.MP3);

                player.setEndOfStreamCallback(() -> {
                    player.setAudioStream(randomStrm.get());
                    player.play();
                });

                player.setVolume(targetVolume);
                player.setAudioStream(randomStrm.get());
                player.play();
            }, e -> logger.error("Could not connect voice connection! {}", e));
        });
    }

    private InputStream generateInputStreamFromFile(String filePath) {
        if (filePath.startsWith(CP_URI_PREFIX)) {
            String actualPath = filePath.substring(CP_URI_PREFIX.length());
            InputStream strm = UserSupport.class.getResourceAsStream(actualPath);
            if (strm == null) {
                throw new IllegalArgumentException("Cannot load hotline music. Resource \"" + filePath + "\" does not exist!");
            }
            return strm;

        } else {
            final var audioFile = new File(filePath);
            try {
                return new FileInputStream(audioFile);
            } catch (FileNotFoundException e) {
                throw new IllegalArgumentException("Cannot load hotline music. File \"" + filePath + "\" does not exist!");
            }
        }
    }

    @Listener
    public void onClientMoved(IQueryEvent.INotification.IClientMoved event) {
        IClient client = event.getTarget();

        if (event.getReasonId() == 0) {
            IChannel channel = dataCache.getChannelMap().getOrDefault(event.getTargetChannelId(), null);

            if (client == null || channel == null) {
                logger.debug("Not notifying client: Channel or client not found.");
                return;
            }

            if (!shouldNotify(client, channel)) {
                logger.debug("Not notifying client: Not required by configuration.");
                return;
            }
            notify(client, channel);
        }
    }

    private boolean shouldNotify(IClient client, IChannel channel) {
        boolean isOwnVoice = voiceConnections.stream()
                .map(c -> c.getVoiceConnectionInformation().getClientUniqueId())
                .anyMatch(uid -> client.getClientUniqueID().equals(uid));
        if (isOwnVoice) {
            return false;
        }

        Integer channelID = channel.getID();

        IConfigNode channelConfig = getConfig().getNode(CONF_CHANNELS_NODE, channelID.toString());
        if (channelConfig.isVirtual()) {
            // Channel not configured
            return false;
        }

        List<Integer> staffGroups =
                channelConfig.getNode(CONF_STAFF_GROUPS_NODE).optValueList(Integer.class);

        List<Integer> pokeStaffGroups =
                channelConfig.getNode(CONF_STAFF_GROUPS_POKE_NODE).optValueList(Integer.class);

        List<Integer> userGroups =
                channelConfig.getNode("user_groups").optValueList(Integer.class);

        // This should be refactored once the framework supports notifications
        // https://gitlab.com/fearnixxgaming/jeakbot/jeakbot-framework/issues/25
        boolean isStaff = staffGroups.stream()
                .anyMatch(i -> client.getGroupIDs().contains(i))
                || pokeStaffGroups.stream()
                .anyMatch(i -> client.getGroupIDs().contains(i));
        if (isStaff) {
            // Staff does not trigger!
            return false;
        }

        // Is not a support-receiving user
        // No restriction? -> Should notify!
        return userGroups.stream()
                .anyMatch(i -> client.getGroupIDs().contains(i));
    }

    private void notify(IClient client, IChannel channel) {

        int count = notifyStaff(client, channel);

        String msgKey = (count > 0 ? "notify.staff.isAvailable" : "notify.staff.isNotAvailable");

        final var params = Map.of(
                "channel", channel.getName(),
                "count", Integer.toString(count),
                "user", client.getNickName()
        );
        String notification = localizationUnit.getContext(client.getCountryCode()).getMessage(msgKey, params);

        bot.getServer().getConnection().sendRequest(
                client.sendMessage(notification)
        );
    }

    private int notifyStaff(IClient user, IChannel channel) {
        IConfigNode channelNode = getConfig().getNode(CONF_CHANNELS_NODE, channel.getID().toString());
        List<Integer> staff =
                channelNode.getNode(CONF_STAFF_GROUPS_NODE).optValueList(Integer.class);
        List<Integer> pokeStaffGroups =
                channelNode.getNode(CONF_STAFF_GROUPS_POKE_NODE).optValueList(Integer.class);

        final var params = Map.of(
                "user", user.getNickName(),
                "channel", channel.getName()
        );

        HashSet<String> notified = new HashSet<>();
        getNotifiableStaffMembersOf(staff)
                .forEach(c -> {
                    final String notification = localizationUnit.getContext(c).getMessage("notify.staff", params);
                    bot.getServer().getConnection().sendRequest(
                            c.sendMessage(notification)
                    );
                    notified.add(c.getClientUniqueID());
                });
        getNotifiableStaffMembersOf(pokeStaffGroups)
                .forEach(c -> {
                    final String notification = localizationUnit.getContext(c).getMessage("notify.staff", params);
                    bot.getServer().getConnection().sendRequest(
                            c.sendPoke(notification)
                    );
                    notified.add(c.getClientUniqueID());
                });

        return notified.size();
    }

    private Stream<IClient> getNotifiableStaffMembersOf(List<Integer> staff) {
        return dataCache
                .getClients()
                .stream()
                .filter(client -> client.getClientType() == IClient.ClientType.VOICE)
                .filter(
                        c -> c.getGroupIDs()
                                .stream()
                                .anyMatch(staff::contains)
                );
    }

    @Override
    protected String getDefaultResource() {
        return null;
    }

    @Override
    protected boolean populateDefaultConf(IConfigNode root) {
        IConfigNode channelsNode = root.getNode(CONF_CHANNELS_NODE);
        channelsNode.getNode("1", CONF_STAFF_GROUPS_NODE).setList();
        channelsNode.getNode("1", CONF_STAFF_GROUPS_POKE_NODE).setList();
        channelsNode.getNode("1", "user_groups").setList();
        channelsNode.getNode("1", CONF_HOTLINE, "music")
                .appendValue("classpath:/usersupport/Local Forecast - Elevator.mp3");
        channelsNode.getNode("1", CONF_HOTLINE, "nickname").setString("usersupport:1");
        channelsNode.getNode("1", CONF_HOTLINE, "volume").setDouble(.01);
        return true;
    }

    @Override
    protected void onDefaultConfigLoaded() {
        super.onDefaultConfigLoaded();
        saveConfig();
    }

    @Listener
    public void onShutdown(IBotStateEvent.IPreShutdown event) {
        voiceConnections.forEach(c -> {
            c.disconnect("Shutting down.");
        });
    }
}
